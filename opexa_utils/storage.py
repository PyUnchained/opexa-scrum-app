from kivy.uix.widget import Widget
try:
    from settings import STORE
except:
    STORE = None

class JsonStoreControl(Widget):
    """
    Controls the JSON store (database) and actually handles saving and
    retrieving data for a value.
    """

    def __init__(self, **kwargs):
        super(JsonStoreControl, self).__init__(**kwargs)
        self.store_key = kwargs.pop('store_key')
        self.store = STORE
        
    def save(self, val, **kwargs):
        self.store.put(self.store_key, val = val)

    def get_value(self):
        if self.store.exists(self.store_key):
            return self.store.get(self.store_key)['val']

        else:
            return None

class JsonStoreElement(Widget):
    def __init__(self, **kwargs):
        super(JsonStoreElement, self).__init__(**kwargs)
        self.control = None
        if 'control' in kwargs:
            self.control = kwargs.pop('control')