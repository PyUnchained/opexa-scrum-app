def get_model_name(model):
	if not isinstance(model, basestring):
            return model.__name__
        else:
            return model