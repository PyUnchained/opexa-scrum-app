from kivy.uix.screenmanager import ScreenManager, Screen
from kivy.uix.button import Button
from kivy.uix.pagelayout import PageLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.scrollview import ScrollView
from kivy.uix.label import Label
from kivy.graphics import Color, Rectangle
from kivy.graphics import BorderImage
from kivy.core.window import Window
from kivy.uix.image import Image, AsyncImage
from kivy.uix.actionbar import*
from kivy.utils import get_color_from_hex
from kivy.uix.screenmanager import*
from kivy.uix.checkbox import CheckBox
from kivy.uix.slider import Slider
from kivy.uix.tabbedpanel import TabbedPanel, TabbedPanelItem, TabbedPanelHeader
from kivy.uix.stacklayout import StackLayout
from kivy.uix.textinput import TextInput
from kivy.utils import get_color_from_hex


from opexa_utils.storage import JsonStoreControl, JsonStoreElement
from exceptions import ImproperlyConfigured
from ui.layouts import RowWrapper, RowLabel, RowInput, GenericView
from kivy.uix.popup import Popup


class WarningPopup(Popup):
    """
    Custom warning Popup. Allows warning messages to easily be displayed as a popup.
    """
    
    def __init__(self, *args, **kwargs):
        message = kwargs.pop('message', None)
        if 'title' not in kwargs.keys():
            kwargs['title'] = 'Warning!'

        if message:
            display_area = BoxLayout(orientation = 'horizontal')
            dismiss_btn = Button(text='Dismiss')
            display_area.add_widget(message)
            display_area.add_widget(dismiss_btn)
            kwargs['content'] = display_area

        super(WarningPopup, self).__init__(*args, **kwargs)
        if message:
            dismiss_btn.bind(on_press=self.dismiss)
            self.open()


def get_color(code):
    if code[0] != '#':
        code = '#' + code

    return get_color_from_hex(code)

class SliderElement(Slider, JsonStoreElement):
    """
    Slider linked to a label.
    """
    def __init__(self, **kwargs):
        super(SliderElement, self).__init__(**kwargs)
        self.linked_label = kwargs.pop('label')

    def on_value(self, *args):
        if hasattr(self, 'linked_label'):
            self.linked_label.update_text(self.value)
            self.control.save(self.value)
        

class LabelElement(Label):
    """
    Label whose text changes based on the information stored in the database.
    """
    def __init__(self, **kwargs):
        super(LabelElement, self).__init__(**kwargs)
        self.template = kwargs.pop('template')
        self.text = self.template.replace('{}', str(kwargs.pop('initial')))

    def update_text(self, value):
        self.text = self.template.replace('{}', str(value))

class CheckBoxElement(JsonStoreElement,CheckBox):
    """
    CheckBox linked to a saved Boolean value.
    """

    def on_state(self, *args):
        if hasattr(self, 'control'):
            self.control.save(self.state)
        super(CheckBoxElement, self).on_state(*args)

class TextInputElement(JsonStoreControl, TextInput):
    """
    A text input that saves the data to the database as it's typed.
    """

    def __init__(self, num = False, **kwargs):
        super(TextInputElement, self).__init__(**kwargs)
        self.default_bg = self.background_color
        self.num = num
        initial = self.get_value()
        if not initial:
            self.text = ''
        else:
            self.text = initial

    def on_text(self, instance, value):
        save_okay = True
        self.background_color = self.default_bg

        if self.num:
            for cha in self.text:
                if not cha.isdigit():
                    self.text = self.text[:-1]
                    save_okay = False
                    break
                

        if save_okay:
            self.save(self.text)


class LinkedCheckBox(JsonStoreControl, StackLayout):
    """A CheckBox linked to a label.
    """

    def __init__(self, **kwargs):
        super(LinkedCheckBox, self).__init__(**kwargs)

        text = kwargs.pop('text')

        #Get initial value from database
        initial = self.get_value()
        if not initial:
            initial = 'normal'

        self.add_widget(
            Label(font_size = '12sp',
                text = text,
                size_hint=(0.7, 1)
                )
        )

        self.add_widget(
            CheckBoxElement(
                size_hint=(0.3, 1),
                control = self,
                state = initial
            )
        )


class LinkedSlider(JsonStoreControl,StackLayout):
    """
    A slider linked to a label. The label must have a template which
    shows where to substitute the value of the slider ({}), and the value will
    change as the slider changes.
    """

    def __init__(self, **kwargs):
        super(LinkedSlider, self).__init__(**kwargs)

        #Get initial value from database
        initial = self.get_value()
        if not initial:
            initial = 0

        template = kwargs.pop('template')

        self.label = LabelElement(font_size = '12sp',
                    halign='left',
                    size_hint=(1, 0.2),
                    template = template,
                    initial = initial
                    )
        self.slider = SliderElement(min=5, max=10, step = 1,
            size_hint=(1, 0.8),
            label = self.label,
            control = self,
            value = initial)

        self.add_widget(self.label)
        self.add_widget(self.slider)

class OpexaScreenManager(ScreenManager):
    """
    Special screen manager used for the app (which, to be fair, actually does nothing at the moment).
    """
    pass


class OpexaScreen(Screen):

    """
    Special screen to be used with the OpexaScreenManager and OpexaApp. 
    These screens are aware of the KivyModels associated with them, how to display them
    and how to organize their data.
    """

    def __init__(self, *args, **kwargs):
        self.model_class = kwargs.pop('model_class', None)
        self.app = kwargs.pop('app', None)

        if self.model_class:
            if isinstance(self.model_class, basestring):
                self.name = self.model_class
                self.model_class = None
            else:       
                self.name = self.model_class.__name__
        else:
            self.name = None

        self.display_area = kwargs.pop('display_area', None)

        super(OpexaScreen, self).__init__(*args, **kwargs)
        if not self.display_area:
            self.display_area = StackLayout(padding = [0, 40, 0, 0])
        self.add_widget(self.display_area)

    def load_model(self, model, view = 'edit', previous_screen = None):
        self.display_area.clear_widgets()

        #Heading
        heading = Label(text = str(model),
                font_size = '20sp',
                font_name = 'Days',
                size_hint_y = 0.1,
                color = self.app.get_color('0052ca'))
        # self.display_area.add_widget(heading)

        # view = GenericView(heading = heading,
        #         model = model,
        #         app = self.app)
        # view.setup_display(display_defaults = {'padding':[20,0,20,0]})

        #Each row of data
        for f in model.get_fields():
            row = RowWrapper(orientation = 'horizontal',
                size_hint_y = 0.08,
                side_padding = 0.3,
                display_defaults = {'padding':[20,0,20,0]})

            label = RowLabel(text = f.capitalize() + ':',
                size_hint_x = 0.3,
                size_hint_y = 0.5,
                halign = 'left')
            label.bind(size=label.setter('text_size'))
            info = RowInput(text = str(getattr(model, f)),
                size_hint_x = 0.3,
                size_hint_y = 0.5)

            row.add_columns(label, info)
            self.display_area.add_widget(row)

        #Buttons at bottom of screen
        btn = self.app.button_class(text = 'Home',
            size_hint_x = 0.5,
            size_hint_y = 0.1)
        btn.bind(on_press=self.app.goto_home)
        self.display_area.add_widget(btn)

        linked_models = []
        if self.app.screen_settings:
            if model.__class__ in self.app.screen_settings.keys():
                settings = self.app.screen_settings[model.__class__]
                if 'link_to_fks' in settings.keys():
                    linked_models = settings['link_to_fks']


        for model_class in linked_models:
            btn = self.app.get_goto_button(model_class, model)
            btn.size_hint_x = 0.5
            btn.size_hint_y = 0.1
            self.display_area.add_widget(btn)

class OpexaActionBar(ActionBar):
    """
    The Android action bar that appears at the top of the screen (Kivy provides no examples
    of how to make this element programatically, so this may turn out to be a hacky solution
    but so far it works and avoids having to create this in the kv language, which
    is more difficult, especially with several different screens).
    """

    def __init__(self, *args, **kwargs):
        settings = kwargs.pop('settings', None)
        if not settings:
            raise ImproperlyConfigured('action bar settings missing.')
            
        super(OpexaActionBar, self).__init__(*args, **kwargs)

        self.pos_hint = {'top':1}

        action_view = ActionView()
        action_previous = ActionPrevious(**settings)
        action_view.add_widget(action_previous)
        self.add_widget(action_view)

        

    