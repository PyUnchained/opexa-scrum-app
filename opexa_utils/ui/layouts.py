from kivy.uix.screenmanager import ScreenManager, Screen
from kivy.uix.button import Button
from kivy.uix.pagelayout import PageLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.scrollview import ScrollView
from kivy.uix.label import Label
from kivy.graphics import Color, Rectangle
from kivy.core.window import Window
from kivy.uix.image import Image, AsyncImage
from kivy.uix.actionbar import*
from kivy.utils import get_color_from_hex
from kivy.uix.screenmanager import*
from kivy.uix.checkbox import CheckBox
from kivy.uix.slider import Slider
from kivy.uix.tabbedpanel import TabbedPanel, TabbedPanelItem, TabbedPanelHeader
from kivy.uix.stacklayout import StackLayout
from kivy.uix.textinput import TextInput
from kivy.utils import get_color_from_hex

class Padding(Label):
	text = ''

class RowDisplay(BoxLayout):
	orientation = 'horizontal'

class GenericView(StackLayout):
	"""
	This is the inter-changeable layout class, think of an HTML form template.
	Ideally, this should be interchangeable, but currently there's an error in how it
	displays for some reason.
	"""

	def __init__(self, *args, **kwargs):
		self.heading = kwargs.pop('heading', 'None')
		self.model = kwargs.pop('model')
		self.app = kwargs.pop('app')
		super(GenericView, self).__init__(*args, **kwargs)

	def setup_display(self, display_defaults = {}):

		self.add_widget(self.heading)

		for f in self.model.get_fields():
			row = RowWrapper(orientation = 'horizontal',
				size_hint_y = 0.08,
				side_padding = 0.3,
				display_defaults = display_defaults)

			label = RowLabel(text = f.capitalize() + ':',
				size_hint_x = 0.3,
				size_hint_y = 0.5,
				halign = 'left')
			label.bind(size=label.setter('text_size'))
			info = RowInput(text = str(getattr(self.model, f)),
				size_hint_x = 0.3,
				size_hint_y = 0.5)

			row.add_columns(label, info)
			self.add_widget(row)

		
		btn = self.app.button_class(text = 'Home',
		    size_hint_x = 0.5,
		    size_hint_y = 0.1)
		btn.bind(on_press=self.app.goto_home)
		self.add_widget(btn)

		linked_models = []
		if self.app.screen_settings:
			if self.model.__class__ in self.app.screen_settings.keys():
				settings = self.app.screen_settings[self.model.__class__]
				if 'link_to_fks' in settings.keys():
					linked_models = settings['link_to_fks']

		for model_class in linked_models:
			btn = self.app.get_goto_button(model_class,
				view_model_pk = self.model.pk)
			btn.size_hint_x = 0.5
			btn.size_hint_y = 0.1
			self.add_widget(btn)




		



class RowWrapper(BoxLayout):
	"""
	Wrapper that sits around a row of data.
	"""
	def __init__(self, *args, **kwargs):
		self.side_padding = kwargs.pop('side_padding', None)
		self.display_defaults = kwargs.pop('display_defaults', {})
		super(RowWrapper, self).__init__(*args, **kwargs)
		
		if self.side_padding == None:
			self.side_padding = 0.2

	def add_columns(self, *args):
		self.add_widget(Padding(
			size_hint_x = self.side_padding/2,
			))


		self.display = RowDisplay(**self.display_defaults)
		for column in args:
			self.display.add_widget(column)
		self.add_widget(self.display)

		self.add_widget(Padding(
			size_hint_x = self.side_padding/2))




class RowLabel(Label):
	"""Label for a row."""
	color = get_color_from_hex('#A1A1A7')

class RowInput(Label):
	"""Text for a row, associated with the KivyModel's data."""
	color = get_color_from_hex('#A1A1A7')