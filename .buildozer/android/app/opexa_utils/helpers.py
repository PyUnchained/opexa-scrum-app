import inspect

from kivy.utils import get_color_from_hex
from kivy.app import App
from kivy.core.window import Window
from kivy.uix.screenmanager import ScreenManager, Screen
from opexa_utils.uix import OpexaScreen, OpexaScreenManager, OpexaActionBar, WarningPopup
from opexa_utils.utils import get_model_name
from exceptions import ImproperlyConfigured
from kivy.adapters.listadapter import ListAdapter
from kivy.uix.listview import ListItemButton, ListView
from kivy.uix.stacklayout import StackLayout
from kivy.uix.button import Button
from kivy.uix.popup import Popup
from kivy.uix.label import Label



class OpexaListView(ListView):

    def __init__(self, *args, **kwargs):
        self.model_queryset = kwargs.pop('model_queryset', None)

        class DataItem(object):
            def __init__(self, text='', is_selected=False):
                self.text = text
                self.is_selected = is_selected

        data_items = [DataItem(text='cat'),
                      DataItem(text='dog'),
                      DataItem(text='frog')]

        list_item_args_converter = lambda row_index, obj: {'text': obj.text,
                                                           'size_hint_y': None,
                                                           'height': 25}

        self.adapter = ListAdapter(data=data_items,
                                   args_converter=list_item_args_converter,
                                   propagate_selection_to_data=True,
                                   cls=ListItemButton)

        super(OpexaListView, self).__init__(*args, **kwargs)

        # if self.model_queryset:
        #     args_converter = lambda row_index, model: {'text': str(model),
        #         'size_hint_y': None,
        #         'height': 25}

        #     self.adapter = ListAdapter(data=self.model_queryset,
        #         args_converter=args_converter,
        #         cls=ListItemButton,
        #         selection_mode='single',
        #         allow_empty_selection=True)



class OpexaApp(App):
    """
    Automatically creates screens for each of the models defined for this app.
    Each screen class is created dynamically, being named after the class name
    of the model. A model called 'Product' would become 'ProductClass', which
    can now be referenced in the kv file.
    """

    models = None
    listview_models = None
    screen_manager = None
    action_bar_settings = None
    home_screen_name = 'Home'
    button_class = Button
    _buttons = []
    screen_settings = None
    warning_popup = WarningPopup

    def __init__(self, *args, **kwargs):
        super(OpexaApp, self).__init__(*args, **kwargs)
            

    def build(self):
        """
        Sets up and initializes the app.
        """
        self.screen_manager = OpexaScreenManager()
        if not self.models:
            raise ImproperlyConfigured("models attribute missing.")

        #Create Home Screen
        self.home_screen = self._add_screen(self.home_screen_name)

        for model_class in self.models:
            print self._add_screen(model_class)

        self.screen_manager.current = self.home_screen_name + 'Screen'
        return self.screen_manager

    def add_list_view(self, model):
        list_class_name = get_model_name(model) + 'ListView'
        ListViewClass = type(list_class_name, (OpexaListView,), dict(model_queryset = None))

        if self.dev_list_view_queryset:
            new_list_view = ListViewClass(
                model_queryset = self.dev_list_view_queryset
                )

    def _add_screen(self, model_class):
        """
        Inserts a new dynamic screen.
        """

        #Dynamically create the class representning the model's screen
        screen_class_name = get_model_name(model_class) + 'Screen'
        ScreenClass = type(screen_class_name, (OpexaScreen,), dict(model_class = None))
        new_screen = ScreenClass(model_class = model_class,
            name = screen_class_name,
            app = self)

        #Add a BoxLayout Widget to display the information


        #Add extra bits to the screen
        if self.action_bar_settings:
            new_screen.add_widget(OpexaActionBar(settings = self.action_bar_settings))

        new_screen.add_widget(StackLayout())

        self.screen_manager.add_widget(new_screen)

        #Create a button to link to this screen later
        self._add_goto_button(model_class)

        return new_screen

    def _add_goto_button(self, model_class):
        """
        Add a button that can later be used to jump to the screen for this model class.
        """
        
        if not isinstance(model_class, basestring):
            btn = self.button_class()
            btn.model_class = model_class
            self._buttons.append(btn)

    def get_goto_button(self, goto_model_class, instance_for_fk):
        """
        Returns a button to switch to a a ModelScreen.
        """
        
        for btn in self._buttons:
            if btn.model_class == goto_model_class:
                btn.model_instance = None
                fk_field = instance_for_fk.fk_field_for_model(goto_model_class)

                for m in self._all_models:
                    try:
                        fk_field_value = getattr(m, fk_field)
                    except:
                        continue

                    if m.__class__ == goto_model_class and fk_field_value == instance_for_fk.pk:
                        btn.model_instance = m
                        text = 'View ' + btn.model_instance.verbose_name_plural
                        btn.text = text
                        btn.goto_target = btn.model_instance
                        btn.bind(on_press=self.goto)
                        return btn

        return None



    @property
    def window_size(self):
        return str(self.window.size)


    def get_color(self, code, *args):
    	"""
    	Converts a hexidecimal number into rgba format. The optional argument represents
        the level of transparency to set.
    	"""

        if code[0] != '#':
            code = '#' + code

        if args:
            color = get_color_from_hex(code)
            color[-1] = args[0]
            return color
        return get_color_from_hex(code)

    def goto(self, instance, screen_name = None):
        """
        Shifts the screen manager to a new screen. The button that calls this function
        must have a 'goto' property with the name of the screen to jump to. Optionally,
        the button can indicate an exact objects screen to view.
        """
        if hasattr(instance, 'goto_target'):
            target_screen_name = instance.model_instance.goto_screen()
            screen = self.get_screen(target_screen_name)

            #If the model isn't None, load its screen...
            if instance.goto_target:
                target_screen_name = instance.goto_target.goto_screen()
                screen.load_model(instance.goto_target, previous_screen = self.screen_manager.current)
                self.screen_manager.current = target_screen_name

            #If the model can't be found...
            else:
                raise Exception('Linked Model Not Found!')

        else:
            #If there's no specific target to switch to....
            target_screen_name = instance.model.goto_screen()
            screen = self.get_screen(target_screen_name)
            screen.load_model(instance.model, previous_screen = self.screen_manager.current)
            self.screen_manager.current = target_screen_name

    def goto_home(self, instance):
        """
        Returns to the app's home screen.
        """
        self.screen_manager.current = self.home_screen_name + 'Screen'

    def get_screen(self, name):
        """
        Either returns the instance representing the current screen or returns None.
        """
        for s in self.screen_manager.screens:
            if s.name == name:
                return s
        return None
