
from exceptions import ImproperlyConfigured

class DataItem(object):
    def __init__(self, text='', is_selected=False):
        self.text = text
        self.is_selected = is_selected


class KivyModel():
	"""
	Representation of a model in Kivy
	"""
	
	fields = None
	pk = None

	def __init__(self, *args, **kwargs):

		if not self.fields:
			raise ImproperlyConfigured('Fields attribute not defined.')

		for f in self.fields:
			if not hasattr(self, f):
				setattr(self, f, None)

		if kwargs:
			for f in kwargs.keys():
				if not hasattr(self, f):
					raise ImproperlyConfigured('Model does not define field: %s' % f)
				setattr(self, f, kwargs[f])

	def goto_screen(self, view = 'edit'):
		"""
		Return the name of the class of screen this object belongs to.
		"""

		if view == 'edit':
			return self.__class__.__name__ + 'Screen'

	def get_fields(self):
		"""
		Returns a list of all model fields that are intended to be displayed
		(all fields excluding the pk field).
		"""

		fields = self.__dict__.keys()
		if 'pk' in fields:
			fields.remove('pk')
		return fields

	def fk_field_for_model(self, model_class):
		"""
		Return the fk field for this model as defined in the other model class.
		"""
		if self.fks:
			for entry in self.fks:
				if entry[0] == model_class.__name__: 
					return entry[1]
		return None 

	@property
	def data_item(self):
		return self.data_item(str(self))



