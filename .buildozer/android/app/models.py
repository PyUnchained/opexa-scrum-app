from opexa_utils.models import KivyModel

class Project(KivyModel):

	fields = ['name', 'complete']
	fks = [('Sprint', 'project')]
	verbose_name = 'Project'
	verbose_name_plural = 'Projects'

	def __str__(self):
		return self.name


class Sprint(KivyModel):

	fields = ['project', 'name', 'goal', 'start', 'end', 'contributors', 'items', 'review_summary',
		'complete']
	verbose_name = 'Sprint'
	verbose_name_plural = 'Sprints'

	def __str__(self):
		return self.name