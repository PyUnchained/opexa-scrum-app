Kivy Models
===========

The most fundamental change is providing an interface by which a Kivy app can easily understand and interact with the data structures that are used in Django. The KivyModels are not designed to be a replacement or direct analogue for Django models. The main goals are as follows\:

#. Provide a consistent interface to read/respond to information provided from a Django server.
#. Provide a consistent interface for the android app to save and retrive data.

The following is an example of a simple KivyModel::

	from opexa_utils.models import KivyModel

	class Product(KivyModel):
		fields = ['name', 'contributors', 'client','administrator']

The ``fields`` attribute corresponds to the names of the corresponding fields in the models defined in Django. Any data that will be transfered between the App and the server should ideally be defined as a KivyModel class.

.. toctree::
    :maxdepth: 2
