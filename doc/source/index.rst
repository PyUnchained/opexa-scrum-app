.. OPEXA Scrum documentation master file, created by
   sphinx-quickstart on Sat Aug 13 22:26:24 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to OPEXA Scrum's documentation!
=======================================

This is the documentation for the App interface for the OPEXA Scrum interface, built in Python and Kivy. In addition there is also a suite of helper classes stored in the opexa_utils module to make development with Kivy easier and simpler. It's primarily for the design team but also contains useful information on how the system is designed to work.

The main focus in on the features that have been added on top of the conventional Kivy functionality. Links to the documentation of the actual code are provided at the bottom of this page.

Overall Design
--------------

The system comes in two parts:

#. Web Server - Provides the administration panel, central database and distributing communication to the app.
#. App - Interface to use the system on mobile, tablet or UNIX systems (with a few tweaks, it should also run on Windows).


.. note::
    Unfortunately, I can only build the App for Android, as making an iOS App requires the iOS operating system. However, the process should be fairly similar (I have never tested this feature, but the documentation seems to indicate that it should work without altering the code), and the source code will be available for Vusa to build and compile to iOS.


.. toctree::
    :maxdepth: 2

    models
    opexa_utils
    source_code/source_code_index

