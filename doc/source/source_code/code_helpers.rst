Helpers
=======

The most important part of the helpers module is the custom OpexaApp, which esentially ties everything together. Every part of the app is made aware of the instance it belongs to, allowing this object to have finer control over the behaviour and contents of the app.

Models
------

It is aware of the KivyModels the app is designed to handle and makes provisions accordingly. This includes loading and keeping track of the data associated with these models, as well as linking them to the correct screen used to display/edit the information.

The major advantage of the way this is done is that it allows very easy control over the behaviour of each KivyModel's screen. Want one model to have a different background than another? Want the edit page of one model to have certain buttons the other one doesn't? Those are the challenges this solves.


Source Code
-----------

.. automodule:: opexa_utils.helpers
   :members:
   :undoc-members: