Storage
=======

Handles what happens in the background when code gets saved to the database. Currently these features haven't been implemented in the demo because the Storage classes have not been updated to work with KivyModel, which would also help to make them a little faster (see note below).

.. note::
	Currently not the most performance friendly, since it updates the database as you type, and this produces a slight delay and abit of quirkyness on lower end devices. Once integrated with models, they would update the KivyModel instance stored in memory, and only save updated changes when the user switches focus to another app.


Source Code
-----------

.. automodule:: opexa_utils.storage
   :members: