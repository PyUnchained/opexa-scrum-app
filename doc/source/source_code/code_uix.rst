UIX
===

This is the user interface, most of the things that people will actually SEE on the app. This is mainly the place to find all of the important custom widgets.

.. note::
	Those code's still a bit of a mess. The code was first located in the ``opexa_utils.uix`` module, but due to the sheer number of elements that will need to be incorporated, the structure is changing to more closely follow that of Kivy itself, meaning all new code is actually in ``opexa_utils.ui``, in a submodule. For example, all layouts are now in ``opexa_utils.ui.layouts``

Screens and Managers
--------------------

Comes with a custom ScreenManager that handles creating screens for each of their models. Each KivyModel's screen is created dynamically as a new class. A KivyModel called 'Product' would become the 'ProductScreen' class, which can then be referenced in the kv file. It's also possible to affect the appearance of ALL the screens, simply by referencing the OpexaScreen class (which is, for example, how all of the screens have the same background).

Using the standard approach would require defining each class of screen manually, which causes issues if the data that needs to be displayed changes. Here's its as simple as informing the OpexaApp of the model to add, and it's done.

.. note::
	Ideally, the screen manager should create 2 different screens, one for editing or creating the the data in a KivyModel, and the second for simply viewing the data. Currently, only the screen for viewing data is created, but the rest of that goodness is being worked on!

It also makes switching between screens easy, as one only needs to supply the name of the screen and (optionally) the KivyModel whose data that screen should display.

The App also creates a HomePage by default, which can then be styled and manipulated as one sees fit, since this page is often very different, whilst pages that display actual information are laregly very similar.

Layouts
-------

Layouts control how data is presented. In particular, these layouts control how data is presented for KivyModel objects. For example, the screens that display Projects and Sprints use the exact layout. You can think of these as the HTML equivalent of a template form.

This form can be overriden for a specific screen, allowing different screens to have a different overall look and feel, though often times a lot of this will be very standard, for example creating a table or listing the information of all the fields is virtually the same for every model, and this allows the code to be written only once (or not at all if you're feeling lazy and just want to go with the standard layout, as in the demo).

Linked UI Elements
------------------

These are UI elements that are linked to a particular value in a KivyModel. For example, if you would like a slider that displays the size of a certain parameter, this is what you're looking for. They automatically handle saving information to the database as well.

Warning Popups
--------------

Another feature is the warning Popups, for example if data can't be found, etc, that the app might need to display on a regular basis, and these provide an easy way for streamlining the process of creating one as well as making sure that there is a 'Dismiss' button (which for some reason is not created by default).


Buttons
-------

One of the other important functions it handles is creating buttons, but also allowing them to be available for easy access, so that any page can easily have the button added to it. It also makes the code a lot more efficient, since the actual buttons are created once at the beginning, not every time the page loads.

The default buttons created so far link to:

#. A go to home-page button.
#. Links to the display pages for all KivyModels defined.
#. A previous page button.


ActionBar
---------

The action bar (at the top of the screen where the OPEXA logo is) is also controlled by the OpexaApp and can be turned on and off, though right now not a whole lot more than that.


Source Code
-----------

.. automodule:: opexa_utils.uix
   :members:

.. automodule:: opexa_utils.ui.layouts
   :members: