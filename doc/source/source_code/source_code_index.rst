The Source
==========

Below is all of the custom source code that keeps the OPEXA app humming. The code is designed to be reused in any other app. Some of the notable features are:

#. Automatically creating screens for any model defined.
#. Functionality of retrieving data on saved models.
#. Automatically generating screens to display a product's information.
#. Managing the creation and style of the buttons that link to other pages.

The source code will be broken down by module in order to make it easier to follow.

.. toctree::
    :maxdepth: 2

    code_helpers
    code_uix
    code_storage