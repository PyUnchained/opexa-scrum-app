OPEXA Utils Module
==================

Provides an interface to create 'Django-esque' models and make use of them within a Kivy app. It helps with handling storage, the user interface and the api for sending and receiving KivyModel and standard Django models between the app and server. Useful in the following cases:

#. Automatically generating screens to create or change the information in a KivyModel.
#. Providing a wrapper to automatically handle storing and retrieving data from a KivyModel.
#. Dynamically create screens that can be referenced from the kv file.


.. toctree::
    :maxdepth: 2

