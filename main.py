import kivy
from random import randint
from random import choice
from string import ascii_uppercase
from datetime import datetime, timedelta

from kivy.app import App
from kivy.lang import Builder
from kivy.uix.screenmanager import ScreenManager, Screen
from kivy.uix.button import Button
from kivy.uix.pagelayout import PageLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.boxlayout import BoxLayout
from  kivy.uix.scrollview import ScrollView
from kivy.uix.label import Label
from kivy.graphics import Color, Rectangle
from kivy.core.window import Window
from kivy.uix.image import Image, AsyncImage
from kivy.uix.actionbar import*
from kivy.utils import get_color_from_hex
from kivy.uix.screenmanager import*
from kivy.uix.checkbox import CheckBox
from kivy.uix.slider import Slider
from kivy.uix.tabbedpanel import TabbedPanel, TabbedPanelItem, TabbedPanelHeader
from kivy.uix.stacklayout import StackLayout
from kivy.storage.jsonstore import JsonStore

from opexa_utils.uix import (LinkedSlider, OpexaScreen, OpexaScreenManager,
    LinkedCheckBox, TextInputElement)
from opexa_utils.helpers import OpexaApp, OpexaListView
from opexa_utils.models import KivyModel

from models import Project, Sprint

from kivy.core.text import LabelBase  
LabelBase.register(name="Days",  
   fn_regular="static/fonts/Days.otf",)
LabelBase.register(name="QC",  
   fn_regular="static/fonts/QC.otf",)

class HomeScreen(OpexaScreen):

    def __init__(self, *args, **kwargs):
        print 'DIGKTITGLTLG'
        super(HomeScreen, self).__init__(self, *args, **kwargs)


class NiceButton(Button):
    pass


class ScrumApp(OpexaApp):
    """
    This is a docstring
    """
    models = [Project, Sprint]
    # dev_list_view_queryset = [Product(name = 'Testy', size = 56)]
    action_bar_settings = {'title': '',
            'with_previous': False,
            'app_icon': 'static/img/opexa_icon.png'}
    button_class = NiceButton

    screen_settings = {Project:{'link_to_fks':[Sprint]}}

    def build(self):
        super(ScrumApp, self).build()

        layout = BoxLayout(orientation = 'vertical',
            padding = [0,50,0,0])
        layout.add_widget(Label(text = 'SCRUM Projects',
            font_size = 30,
            size_hint_y= 0.3,
            color = self.get_color('378cff'),
            font_name = 'QC'))

        lower_layout = BoxLayout(orientation = 'horizontal',
            size_hint_y= 0.9)
        layout.add_widget(lower_layout)

        lower_layout_left = StackLayout()
        lower_layout_right = BoxLayout(orientation = 'vertical')
        lower_layout.add_widget(lower_layout_left)
        lower_layout.add_widget(lower_layout_right)
        for item in self._testing_models():
            if item.__class__ == Project:
                label = item.name
                btn = NiceButton(text = label,
                    size_hint = (1,0.2))
                btn.model = item
                btn.bind(on_press=self.goto)
                lower_layout_left.add_widget(btn)
        

        self.home_screen.add_widget(layout)
        return self.screen_manager

    def _testing_models(self):
        testing_models = []
        testing_models.append(Project(**{'name':'First Test Project', 'complete':95.4, 'pk':1}))
        testing_models.append(Project(**{'name':'Another', 'complete':32.4, 'pk':2}))
        testing_models.append(Project(**{'name':'Some Other Thing', 'complete':9.4,  'pk':3}))
        testing_models.append(Sprint(**{'name':'First Sprint', 'goal':'Awesome Sauce',  'project':1}))
        testing_models.append(Sprint(**{'name':'Second Sprint', 'goal':'Good',  'project':2}))
        testing_models.append(Sprint(**{'name':'Another Sprint', 'goal':'Things that could be achieved',
            'project':3}))
        self._all_models = testing_models
        return testing_models


if __name__ == '__main__':
    ScrumApp().run()